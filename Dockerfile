FROM python:3.11.4-alpine

RUN apk update && \
    apk add build-base && \
    apk add libpq-dev
RUN mkdir /app
WORKDIR /app

COPY . .

RUN pip install -r requirements.txt

ENTRYPOINT uvicorn api.main:app --reload --host 0.0.0.0 --port 3000