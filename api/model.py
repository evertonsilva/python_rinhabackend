from datetime import date
from typing import Optional
from uuid import UUID
from pydantic import BaseModel

class Pessoa(BaseModel):
    """Model Pessoa"""
    id: Optional[UUID] | None = None
    apelido: str
    nome: str
    nascimento: date
    stack: list[str] | None = None