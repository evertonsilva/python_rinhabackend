import psycopg2 as pg
from uuid import UUID
from api.model import Pessoa

def dbconnect():
    """ Connect to the PostgreSQL database server """
    conn = pg.connect(
        host="postgres",
        database="postgres",
        user="postgres",
        password="str0ngP@sswd"
    )
    return conn


def save(pessoa: Pessoa):
    pessoa_id = None
    try:
        sql = "INSERT INTO public.pessoas (apelido, nome, nascimento, stack)  VALUES (%s, %s, %s, %s) RETURNING id;"
        conn = dbconnect()
        cur = conn.cursor()
        
        cur.execute(sql, (pessoa.apelido, pessoa.nome, pessoa.nascimento, pessoa.stack))
        pessoa_id = cur.fetchone()[0]
        
        conn.commit()
        cur.close()
    except (Exception, pg.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    print(pessoa_id)
    return pessoa_id


def find(id: UUID):
    pessoa = None
    try:
        sql = f"SELECT * FROM public.pessoas WHERE id = '{id}'"
        conn = dbconnect()
        cur = conn.cursor()

        cur.execute(sql)
        res = cur.fetchone()
        pessoa = Pessoa(id=res[0], apelido=res[1], nome=res[2], nascimento=res[3], stack=res[4])
        cur.close()
    except (Exception, pg.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return pessoa


def search(term: str):
    result = None
    try:
        sql = f"""
            SELECT * 
            FROM public.pessoas 
            WHERE apelido LIKE '%{str.lower(term)}%'
            OR lower(nome) LIKE '%{str.lower(term)}%'
        """
        conn = dbconnect()
        cur = conn.cursor()

        cur.execute(sql)
        result = cur.fetchall()
        print(result)

        cur.close()
    except (Exception, pg.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return result


def count():
    result = 0
    try:
        sql = "SELECT count(1) FROM public.pessoas"
        conn = dbconnect()
        cur = conn.cursor()

        cur.execute(sql)
        result = cur.fetchone()
        
        print(result)
        cur.close()
    except (Exception, pg.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return result