from fastapi import FastAPI, Response
from uuid import UUID
from api.database import save, find, search, count
from api.model import Pessoa

app = FastAPI()


@app.get("/")
async def root():
    return { "status": "OK" }


@app.post("/pessoas")
async def salvar_pessoa(pessoa: Pessoa, resp: Response):
    """Insere uma pessoa no banco de dados"""
    try:
        id = save(pessoa)
        resp.status_code = 201
        return { "data": id }
    except:
        resp.status_code = 500
        return { "error": "shit happens =/" }


@app.get("/pessoas/{id}")
async def consultar_pessoa(id: UUID, resp: Response) -> Pessoa:
    """Busca uma pessoa pelo ID"""
    try:
        pessoa = find(id)
        return pessoa
    except:
        resp.status_code = 500
        return { "error": "shit happens =/" }


@app.get("/pessoas")
async def pesquisar(t: str):
    """Pesquisa pessoas por termo de busca"""
    result = search(t)
    return result


@app.get('/contagem-pessoas')
async def contage():
    """Número de registros de pessoas no banco de dados"""
    return count()