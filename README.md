# API Python

Exemplo de API em Python utilizando [FastAPI](https://fastapi.tiangolo.com/) com banco de dados PostgreSQL.

## Executando o projeto

Para executar o projeto rode o comando: `docker-compose up -d`. O comando vai construir dois containers, um para executar a API (rinha_api) e outro para disponibilizar o banco de dados (rinha_db).

Feito isso acesse [localhost:3000/docs](http://localhost:3000/docs) para acessar a página com a documentação da API. O banco de dados poderá ser acessado via `localhost:5432` utilizando algum cliente SQL como o DBeaver ou PgAdmin.  

## Organização do projeto

- `api/main.py` módulo principal e ponto de entrada da aplicação e define todas as rotas da API;
- `api/model` módulo com o modelo definido para representar uma Pessoa;
- `api/database` módulo com as funções de acesso ao banco de dados.


## Banco de dados

Um dos requisitos da Rinha é que o ID dos registros utilizem como valor UUID (Universally Unique Identifier). Para isso é necessário instalar uma extensão no PostgreSQL com a _query_ a seguir:

```sql
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```
Mais detalhes em [https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-uuid/](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-uuid/)

Após habilitar a extensão é utilize o script a baixo para criar a tabela Pessoas

```sql
CREATE TABLE IF NOT EXISTS pessoas (
   id uuid DEFAULT uuid_generate_v4(),
   apelido VARCHAR(32) NOT NULL,
   nome VARCHAR(100) NOT NULL,
   nascimento DATE,
   stack TEXT[],

   PRIMARY KEY (id)
);
```